import React, { useMemo } from 'react';

const List = ({ items }) => {
    const sortedItems = useMemo(() => {
        console.log("list run");
        return items.slice().sort((a, b) => b - a); // Use slice() to avoid mutating the original array
    },[items]);
    const listItems = sortedItems;

    return (
        <ul>
            {listItems.map((item) => (
                <li key={item}>{item}</li>
            ))}
        </ul>
    );
};

export default List;