import { useCallback, useMemo, useState } from "react";
import "./App.css";
import Button from "./components/UI/Button";
import MyElement from "./components/MyElement";
import List from "./components/List";

function App() {
  const [toggleParagraph, setToggleParagraph] = useState(false);
  const [toggleAllow, setToggleAllow] = useState(false);
  const [title, setTitle] = useState("Title");
  console.log("app çalıştı");
const changeTitleHandler = useCallback(()=> {
  setTitle("Title updated");
}
,[]);

const listItems = useMemo(()=> [1,2,3,4,5], []);

  const toggleParapraphHandler = useCallback( () => {
    if(toggleAllow){
      setToggleParagraph((prevState) => !prevState);
    } 
  }
,[toggleAllow]);

const toogleAllowHandler = ()=> {
  setToggleAllow(true);
}

  return (
    <div className="container">
      <h1>Hello</h1>
      <MyElement show={toggleParagraph} />
      <br />
      <br />
      <Button onClick={toogleAllowHandler}>Allow Toggling </Button>
      <br></br>
      <Button onClick={toggleParapraphHandler}>Toggle Paragraph</Button>
      <br>
      </br> <br>
      </br> <br>
      </br>
      <Button onClick={changeTitleHandler}>  Title Change</Button>
      <List items={[1,2,3,4,5]}></List>
    </div>
  );
}

export default App;
